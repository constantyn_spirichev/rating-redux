import {createStore, applyMiddleware} from 'redux';
import reducer from './reducer';
import logger from './middlewares/logger';
import async from './middlewares/async';
import ws from './middlewares/ws';
const rating = {loading: false, entities: [{id: 1, points: 100, name: 'Hello'}]};

const createStoreWithMiddleware = applyMiddleware(logger, async, ws)(createStore)


export default createStoreWithMiddleware(reducer, { rating });

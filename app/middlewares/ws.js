import {setRatingUpdate} from "../reducer";

const middleware = store => next => action => {

  const [loadingType, updateType] = setRatingUpdate().types;

  if(action.type === loadingType) {
    const ws = new WebSocket('ws://rating.smartjs.academy/rating');

    ws.addEventListener('message', (e)=>{store.dispatch(
            {
                type: updateType,
                data: JSON.parse(e.data),
            }
        )
    });
  }

  //console.log("ws MW:", action);
    next(action);
}

export default middleware;

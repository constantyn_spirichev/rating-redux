const RATING_LOADED = 'RATING_LOADED';
const RATING_LOADING = 'RATING_LOADING';
const RATING_UPDATE = 'RATING_UPDATE';
const RATING_UPDATE_CONNECTED = 'RATING_UPDATE_CONNECTED';

export default function reducer(oldState, action) {
  switch (action.type) {
    case RATING_LOADING:
      return {
        ...oldState,
        rating: {
          ...oldState.rating,
          loading: true,
        }
      };

    case RATING_LOADED:
      
      if(oldState.rating.updates && oldState.rating.updates.length) {
        
        const updates = oldState.rating.updates.filter((update)=>{
          if(update.fromVersion == action.data.version){
            action.data.version = update.toVersion;
            action.data.records = applyUpdates(update.updates, action.data.records);
            return false;
          }
          return true;
        });
      }

      return {
        ...oldState,
        rating: {
          ...oldState.rating,
          loading: false,
          version: action.data.version,
          entities: action.data.records,
          updates
        }
      };
    case RATING_UPDATE:
        
        var updatedRating = {...oldState.rating};
        var updates = updatedRating.updates || [];

        if(oldState.rating.loading){
          updates.push(action.data);
        } else {
          if(action.data.fromVersion == oldState.rating.version){
            updatedRating.version = action.data.toVersion;
            updatedRating.entities = applyUpdates(action.data.updates, oldState.rating.entities);
            updatedRating.lastUpdate = action.data;
          }
        }
          
        return {
            ...oldState,
            rating: {
                ...updatedRating,
                updates,
            }

        };
    default:
      return oldState;
  }
}

export function loadRatingData(url = 'http://rating.smartjs.academy/rating?hardMode') {
      return {
        types: [RATING_LOADING, RATING_LOADED],
        request: function () {
          return fetch(url).then(r => r.json())
        }
    }
}

export function setRatingUpdate() {
      return {
        types: [RATING_LOADING, RATING_UPDATE],
    }
}

function applyUpdates(source, target, property = 'id') {
  source.map((srcItem)=>{
    target = target.map((targetItem)=>{
      if(srcItem[property] == targetItem[property]){
        targetItem.points = srcItem.points;
      }
      return targetItem;
    })
  });
  return target;
}

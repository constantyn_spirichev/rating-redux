import BaseView from './BaseView'

export default class RatingView extends BaseView {

  renderItem(item) {
    const li = document.createElement('li');
    li.id = item.id;
    li.textContent = `${item.name} - ${item.points}`;
    return li;
  }

  render(items) {
    if (items.loading) {
      this._node.innerHTML = '<h1>Loading...</h1>';
      return;
    }

    const ol = document.createElement('ol');
    items.entities
      .sort((a, b) => b.points - a.points)
      .forEach(
        item => ol.appendChild(this.renderItem(item)
      ));

    this._node.innerHTML = '';
    this._node.appendChild(ol);
  }
}

import BaseView from './BaseView'

export default class VersionView extends BaseView {

  render(version) {
    if(version)
    this._node.innerHTML = `current version: <span class="blink">${version}</span>`;
    
  }
}

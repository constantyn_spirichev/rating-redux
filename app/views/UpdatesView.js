import BaseView from './BaseView'

export default class UpdatesView extends BaseView {

  render(rating) {
    const update = rating.lastUpdate ;
    if (!update) return;
    
      if(rating.version === update.toVersion){
        update.updates.forEach((item)=>{
          //this._node.querySelectorAll(".update").classList.remove("updated");
          document.getElementById(item.id).classList.add("updated");
        })
      }
    
  }
}

// rating.smartjs.academy/rating
import {loadRatingData, setRatingUpdate} from './reducer';
import store from './store';
import RatingView from './views/RatingView';
import UpdatesView from './views/UpdatesView';
import VersionView from './views/VersionView';
import connect from './connect';

console.log(setRatingUpdate)

const ratingView = connect(
  store,
  new RatingView(document.querySelector('.rating')),
  (state) => state.rating
);

const updatesView = connect(
  store,
  new UpdatesView(document.querySelector('.rating')),
  (state) => state.rating
);

const versionView = connect(
  store,
  new VersionView(document.querySelector('.updates-info')),
  (state) => state.rating.version
);

store.dispatch(loadRatingData());